package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void testTake() {
        // BEGIN
        List<Integer> numbers1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        List<Integer> numbers2 = new ArrayList<>(Arrays.asList(7, 3, 10));
        List<Integer> numbers3 = new ArrayList<>(Arrays.asList());
        int int1 = 2;
        int int2 = 8;
        int int3 = 5;
        List<Integer> result1 = new ArrayList<>(Arrays.asList(1, 2));
        List<Integer> result2 = new ArrayList<>(Arrays.asList(7, 3, 10));
        Assertions.assertEquals(App.take(numbers1, int1), result1);
        Assertions.assertEquals(App.take(numbers2, int2), result2);
        Assertions.assertTrue(App.take(numbers3, int3).isEmpty());
        // END
    }
}
