package exercise;

// BEGIN
public class ReversedSequence implements CharSequence {
    String inputString;

    public ReversedSequence(String inputString) {
        this.inputString = new StringBuilder(inputString).reverse().toString();
    }

    @Override
    public int length() {
        return this.inputString.length();
    }

    @Override
    public char charAt(int index) {
        return this.inputString.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return this.inputString.subSequence(start, end);
    }

    @Override
    public String toString() {
        return this.inputString;
    }
}
// END
