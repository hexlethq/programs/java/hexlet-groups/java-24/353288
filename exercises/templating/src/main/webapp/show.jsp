<%@ page import="java.util.Map" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- BEGIN -->
<html>
<head>
    <title>User</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>table, th, td {border: 1px solid black;}</style>
</head>
<body>
<%
    Map<String, String> user = (Map<String, String>) request.getAttribute("user");
%>
<table>
    <tr>
        <a href="delete?id=<%= user.get("id")%>">Delete this user</a>
    </tr>
    <tr>
        <td><%= user.get("id")%></td>
        <td><%= user.get("firstName")%></td>
        <td><%= user.get("lastName")%></td>
        <td><%= user.get("email")%></td>
    </tr>
</table>
</body>
</html>
<!-- END -->
