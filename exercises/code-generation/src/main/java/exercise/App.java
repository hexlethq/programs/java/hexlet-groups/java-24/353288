package exercise;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

// BEGIN
public class App {
    public static void save(Path path, Car car) {
        try {
            Files.write(path, car.serialize().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Car extract(Path path) {
        Car car = new Car();
        try {
            car = Car.unserialize(Files.readString(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return car;
    }
}
// END
