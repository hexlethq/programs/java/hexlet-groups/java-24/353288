package exercise.controller;
import com.querydsl.core.types.Predicate;
import exercise.model.User;
import exercise.model.QUser;
import exercise.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;


// Зависимости для самостоятельной работы
//import org.springframework.data.querydsl.binding.QuerydslPredicate;
// import com.querydsl.core.types.Predicate;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserRepository userRepository;

    // BEGIN

    //Задачи

//    @GetMapping("")
//    public Iterable<User> getUserByFirstAndLastNames(String firstName, String lastName){
//        if (firstName == null && lastName == null) {
//            return this.userRepository.findAll();
//        }
//        if (firstName == null) {
//            firstName = "";
//        }
//        if (lastName == null) {
//            lastName = "";
//        }
//        return userRepository.findAll(QUser.user.firstName.containsIgnoreCase(firstName)
//                .and(QUser.user.lastName.containsIgnoreCase(lastName)));
//    }

    //Дополнительная задача

    @GetMapping("")
    public Iterable<User> getUserByFirstAndLastNames(
            @QuerydslPredicate(root = User.class) Predicate predicate) {
        return userRepository.findAll(predicate);
    }
    // END
}

