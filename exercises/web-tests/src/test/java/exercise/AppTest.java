package exercise;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import io.javalin.Javalin;
import io.ebean.DB;
import io.ebean.Transaction;

import exercise.domain.User;
import exercise.domain.query.QUser;

class AppTest {

    private static Javalin app;
    private static String baseUrl;
    private static Transaction transaction;

    // BEGIN
    @BeforeAll
    public static void beforeAll() {
        app = App.getApp();
        app.start(0);
        int port = app.port();
        baseUrl = "http://localhost:" + port;
    }

    @AfterAll
    public static void afterAll() {
        app.stop();
    }
    // END

    // Хорошей практикой является запуск тестов с базой данных внутри транзакции.
    // Перед каждым тестом транзакция открывается,
    @BeforeEach
    void beforeEach() {
        transaction = DB.beginTransaction();
    }

    // А после окончания каждого теста транзакция откатывается
    // Таким образом после каждого теста база данных возвращается в исходное состояние,
    // каким оно было перед началом транзакции.
    // Благодаря этому тесты не влияют друг на друга
    @AfterEach
    void afterEach() {
        transaction.rollback();
    }

    @Test
    void testRoot() {
        HttpResponse<String> response = Unirest.get(baseUrl).asString();
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void testUsers() {

        // Выполняем GET запрос на адрес http://localhost:port/users
        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users")
            .asString();
        // Получаем тело ответа
        String content = response.getBody();

        // Проверяем код ответа
        assertThat(response.getStatus()).isEqualTo(200);
        // Проверяем, что на станице есть определенный текст
        assertThat(content).contains("Wendell Legros");
        assertThat(content).contains("Larry Powlowski");
    }

    @Test
    void testUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/5")
            .asString();
        String content = response.getBody();

        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(content).contains("Rolando Larson");
        assertThat(content).contains("galen.hickle@yahoo.com");
    }

    @Test
    void testNewUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/new")
            .asString();

        assertThat(response.getStatus()).isEqualTo(200);
    }

    // BEGIN
    @Test
    void testCreateUser() {

        HttpResponse<String> responsePost = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "TestFirstName")
                .field("lastName", "TestLastName")
                .field("email", "testmail@gmail.com")
                .field("password", "7777777")
                .asString();

        assertThat(responsePost.getStatus()).isEqualTo(302);

        User createdUser = new QUser()
                .lastName.equalTo("TestLastName")
                .findOne();

        assertThat(createdUser).isNotNull();
        assertThat(createdUser.getFirstName()).isEqualTo("TestFirstName");
        assertThat(createdUser.getLastName()).isEqualTo("TestLastName");
        assertThat(createdUser.getEmail()).isEqualTo("testmail@gmail.com");
        assertThat(createdUser.getPassword()).isEqualTo("7777777");
    }

    @Test
    void testCreateUserFailed() {
        HttpResponse<String> responsePost1 = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "")
                .field("lastName", "TestLastName1")
                .field("email", "testmai1l@gmail.com")
                .field("password", "7777777")
                .asString();

        HttpResponse<String> responsePost2 = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "TestFirstName1")
                .field("lastName", "")
                .field("email", "testmail1@gmail.com")
                .field("password", "7777777")
                .asString();

        HttpResponse<String> responsePost3 = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "TestFirstName1")
                .field("lastName", "TestLastName1")
                .field("email", "testmailgmail.com")
                .field("password", "7777777")
                .asString();

        HttpResponse<String> responsePost4 = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "TestFirstName1")
                .field("lastName", "TestLastName1")
                .field("email", "testmail1@gmail.com")
                .field("password", "777")
                .asString();

        assertThat(responsePost1.getStatus()).isEqualTo(422);
        assertThat(responsePost2.getStatus()).isEqualTo(422);
        assertThat(responsePost3.getStatus()).isEqualTo(422);
        assertThat(responsePost4.getStatus()).isEqualTo(422);

        User createdUser1 = new QUser()
                .lastName.equalTo("TestLastName1")
                .findOne();

        User createdUser2 = new QUser()
                .firstName.equalTo("TestFirstName1")
                .findOne();

        assertThat(createdUser1).isNull();
        assertThat(createdUser2).isNull();

        String content1 = responsePost1.getBody();
        String content2 = responsePost2.getBody();
        String content3 = responsePost3.getBody();
        String content4 = responsePost4.getBody();

        assertThat(content1).contains("TestLastName1").contains("Имя не должно быть пустым");
        assertThat(content2).contains("testmail1@gmail.com").contains("Фамилия не должна быть пустой");
        assertThat(content3).contains("7777777").contains("Должно быть валидным email");
        assertThat(content4).contains("TestFirstName1").contains("Пароль должен содержать не менее 4 символов");
    }
    // END
}
