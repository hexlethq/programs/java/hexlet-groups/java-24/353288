package exercise;

// BEGIN
public class MinThread extends Thread {
    private int min = Integer.MAX_VALUE;
    private int[] inputArr;

    public MinThread(int[] numbers) {
        this.inputArr = numbers;
    }

    /**
     *
     */
    @Override
    public void run() {

        for (int i : inputArr) {
            if (i < min) {
                min = i;
            }
        }

//        super.run();
    }

    /**
     * @return min
     */
    public int getMin() {
        return min;
    }
}
// END
