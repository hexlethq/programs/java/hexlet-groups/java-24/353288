package exercise;

import lombok.*;

// BEGIN
@AllArgsConstructor
@NoArgsConstructor
@Getter
// END
class User {
    int id;
    String firstName;
    String lastName;
    int age;
}
