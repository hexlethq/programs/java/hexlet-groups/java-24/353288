package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public class Disconnected implements Connection {
    private TcpConnection tcpConnection;

    public Disconnected(TcpConnection tcpConnection) {
        this.tcpConnection = tcpConnection;
    }

    @Override
    public String currentStateOfConnection() {
        return "disconnected";
    }

    @Override
    public void makeConnection() {
        this.tcpConnection.setState(new Connected(this.tcpConnection));
    }

    @Override
    public void makeDisconnection() {
        System.out.println("Error! Already disconnected");
    }

    @Override
    public void writeData(String data) {
        System.out.println("Error! No connection");
    }
}
// END
