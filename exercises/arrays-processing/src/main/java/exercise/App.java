package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    private static final int EMPTY_ARRAY = -1;
    private static final int NUMBER_OF_TESTS = 5;

    private static final int[] TEST_ARRAY_1 = {1, 2, 3, 4, 5, 6, 7};
    private static final int EXPECTED_RESULT_1 = 20;

    private static final int[] TEST_ARRAY_2 = {0, 0, 0, 0, 0, 0, 0};
    private static final int EXPECTED_RESULT_2 = 0;

    private static final int[] TEST_ARRAY_3 = {};
    private static final int EXPECTED_RESULT_3 = EMPTY_ARRAY;

    private static final int[] TEST_ARRAY_4 = {-5, -1, -10, 34, 0, -4, 12};
    private static final int EXPECTED_RESULT_4 = 0;

    private static final int[] TEST_ARRAY_5 = {22, -4, 33, -56, 28, 12, 35};
    private static final int EXPECTED_RESULT_5 = 40;

    public static int[] getElementsLessAverage(final int[] inputArray) {
        int counter = 0;
        int outIndex = 0;
        double resultAverage = averageOfArrayElements(inputArray);

        if (inputArray.length == 0) {
            return inputArray;
        }
        for (int i = 0; i < inputArray.length; i++) {
            if (inputArray[i] < resultAverage) {
                counter++;
            }
        }
        int[] outputArray = new int[counter];
        for (int i = 0; i < inputArray.length; i++) {
            if (inputArray[i] < resultAverage) {
                outputArray[outIndex] = inputArray[i];
                outIndex++;
            }
        }
        return outputArray;
    }

    public static double averageOfArrayElements(final int[] arrayForAverage) {
        double sumOfElements = 0.0;
        double average = 0.0;
        for (int i = 0; i < arrayForAverage.length; i++) {
            sumOfElements = sumOfElements + arrayForAverage[i];
        }
        average = sumOfElements / arrayForAverage.length;
        return average;
    }

    public static int getSumBeforeMinAndMax(final int[] inputArray2) {
        int sum = 0;
        if (inputArray2.length == 0) {
            return EMPTY_ARRAY;
        }
        int maxValue = Integer.MIN_VALUE;
        int minValue = Integer.MAX_VALUE;
        int indexOfMaxValue = -1;
        int indexOfMinValue = -1;
        for (int i = 0; i < inputArray2.length; i++) {
            if (inputArray2[i] > maxValue) {
                maxValue = inputArray2[i];
                indexOfMaxValue = i;
            }
            if (inputArray2[i] < minValue) {
                minValue = inputArray2[i];
                indexOfMinValue = i;
            }
        }
        if (indexOfMaxValue != indexOfMinValue) {
            int[] croppedArray = null;
            int minIndex = Math.min(indexOfMaxValue, indexOfMinValue);
            int maxIndex = Math.max(indexOfMaxValue, indexOfMinValue);
            croppedArray = Arrays.copyOfRange(inputArray2, minIndex, maxIndex);
            for (int i = 0; i < croppedArray.length; i++) {
                sum = sum + croppedArray[i];
            }
            sum = sum - croppedArray[0];
        }
        return sum;
    }

    public static String testGetSumBeforeMinAndMax() {
        int testPassedCounter = 0;
        int testFailedCounter = 0;

        if (getSumBeforeMinAndMax(TEST_ARRAY_1) == EXPECTED_RESULT_1) {
            testPassedCounter++;
        } else {
            return Arrays.toString(TEST_ARRAY_1) + " result " + getSumBeforeMinAndMax(TEST_ARRAY_1)
                    + " but expected " + EXPECTED_RESULT_1;
        }
        if (getSumBeforeMinAndMax(TEST_ARRAY_2) == EXPECTED_RESULT_2) {
            testPassedCounter++;
        } else {
            return Arrays.toString(TEST_ARRAY_2) + " result "
                    + getSumBeforeMinAndMax(TEST_ARRAY_2) + " but expected " + EXPECTED_RESULT_2;
        }
        if (getSumBeforeMinAndMax(TEST_ARRAY_3) == EXPECTED_RESULT_3) {
            testPassedCounter++;
        } else {
            return Arrays.toString(TEST_ARRAY_3) + " result "
                    + getSumBeforeMinAndMax(TEST_ARRAY_3) + " but expected " + EXPECTED_RESULT_3;
        }
        if (getSumBeforeMinAndMax(TEST_ARRAY_4) == EXPECTED_RESULT_4) {
            testPassedCounter++;
        } else {
            return Arrays.toString(TEST_ARRAY_4) + " result "
                    + getSumBeforeMinAndMax(TEST_ARRAY_4) + " but expected " + EXPECTED_RESULT_4;
        }
        if (getSumBeforeMinAndMax(TEST_ARRAY_5) == EXPECTED_RESULT_5) {
            testPassedCounter++;
        } else {
            return Arrays.toString(TEST_ARRAY_5) + " result "
                    + getSumBeforeMinAndMax(TEST_ARRAY_5) + " but expected " + EXPECTED_RESULT_5;
        }
        testFailedCounter = NUMBER_OF_TESTS - testPassedCounter;
        return testPassedCounter + " tests passed, " + testFailedCounter + " tests failed";
    }
//     END
}


