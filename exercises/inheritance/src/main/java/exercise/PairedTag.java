package exercise;

import java.util.Map;
import java.util.List;

// BEGIN
public class PairedTag extends Tag {
    String body;
    List<Tag> children;

    public PairedTag(String name, Map<String, String> attributes, String body, List<Tag> children) {
        super(name, attributes);
        this.body = body;
        this.children = children;
    }

    @Override
    public String toString() {
        String result = "<" + name + " ";
        for (Map.Entry entry : attributes.entrySet()) {
            result = result + entry.getKey() + "=\"" + entry.getValue() + "\" ";
        }
        result = result.substring(0, result.length() - 1) + ">" + body;
        for (Tag st : children) {
            result = result + st.toString();
        }
        result = result + "</" + name + ">";
        return result;
    }
}
// END
