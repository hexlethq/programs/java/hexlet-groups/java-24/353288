package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        // BEGIN
        List<String> companies = new ArrayList<String>(getCompanies());
        PrintWriter pw = response.getWriter();
        if (request.getQueryString() == null || request.getParameter("search").equals("")) {
            companies.forEach(company -> pw.println(company));
        } else if (!companies.toString().contains(request.getParameter("search"))) {
            pw.write("Companies not found");
        } else {
            pw.write(companies.stream()
                    .filter(x -> x.contains(request.getParameter("search")))
                    .collect(Collectors.joining("\n")));
        }
        pw.close();
        // END
    }
}
