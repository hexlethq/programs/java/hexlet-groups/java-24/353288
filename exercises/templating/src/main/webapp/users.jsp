<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- BEGIN -->
<html>
<head>
    <title>Users</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>table, th, td {border: 1px solid black;}</style>
</head>
<body>
<%
    List<Map<String, String>> users = (List<Map<String, String>>) request.getAttribute("users");
%>
<table>
    <%
      for (int i = 0; i < users.size(); i++) {
    %>
    <tr>
        <td><%= users.get(i).get("id")%></td>
        <td><a href="users/show?id=<%= users.get(i).get("id")%>"><%= users.get(i).get("lastName")%></a></td>
    </tr>
    <%
        }
    %>
</table>
</body>
</html>
<!-- END -->
