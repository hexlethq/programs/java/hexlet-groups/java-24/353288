package exercise;

// BEGIN
public class Circle {
    Point point;
    int radius;

    public Circle(Point point, int radius) {
        this.point = point;
        this.radius = radius;
    }

    public int getRadius() {
        return this.radius;
    }

    public double getSquare() throws NegativeRadiusException {
        double result = Math.PI * Math.pow(radius, 2);
        if (radius < 0) {
//            throw NegativeRadiusExceptions.NEGATIVE_RADIUS_EXCEPTION;
            throw new NegativeRadiusException("Не удалось посчитать площадь");
        }
        return result;
    }
}
// END
