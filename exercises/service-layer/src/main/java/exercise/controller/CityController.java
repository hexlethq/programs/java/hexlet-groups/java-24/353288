package exercise.controller;
import exercise.CityNotFoundException;
import exercise.model.City;
import exercise.repository.CityRepository;
import exercise.service.WeatherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;


@RestController
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private WeatherService weatherService;

    // BEGIN
    @GetMapping("/cities/{id}")
    public Map<String, String> getWeatherInCityById (@PathVariable("id") long id) {
        City city = cityRepository.findById(id).orElseThrow(
                () -> new CityNotFoundException("City not found")
        );
        return weatherService.getWeatherByCityName(city.getName());

    }

    @GetMapping("/search")
    public List<Map<String, String>> getTemperatureByCityName (
            @RequestParam(required = false) String name) {
        List<Map<String, String>> weatherInCities = new ArrayList<>();
        Iterable<City> cities;
        if (name == null) {
            cities = cityRepository.findAllByOrderByName();
            for (City c: cities) {
                weatherInCities.add(weatherService.getTemperatureByCityName(c.getName()));
            }
            return weatherInCities;
        } else {
            cities = cityRepository.findCitiesByNameIsStartingWithIgnoreCase(name);
            for (City c: cities) {
                weatherInCities.add(weatherService.getTemperatureByCityName(c.getName()));
            }
            return weatherInCities;
        }
    }
    // END
}

