package exercise;

class Converter {
    // BEGIN
    public static int convert(final int num, final String unit) {
        final int bInKb = 1024;
        switch (unit) {
            case "Kb": return num / bInKb;
            case "b": return num * bInKb;
            default: return 0;
        }
    }

    public static void main(String[] args) {
        final int inputKb = 10;
        System.out.println(inputKb + " Kb = " + convert(inputKb, "b") + " b");
    }
    // END
}
