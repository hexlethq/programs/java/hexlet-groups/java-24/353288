// BEGIN
package exercise.geometry;

// END
public class Segment {
    private static double[][] segment = new double[2][2];
    private static double[] beginPoint = new double[2];
    private static double[] endPoint = new double[2];

    public static double[][] makeSegment(final double[] point1, final double[] point2) {
        segment[0][0] = point1[0];
        segment[0][1] = point1[1];
        segment[1][0] = point2[0];
        segment[1][1] = point2[1];
        return segment;
    }

    public static double[] getBeginPoint(final double[][] inputSegment) {
        beginPoint[0] = inputSegment[0][0];
        beginPoint[1] = inputSegment[0][1];
        return beginPoint;
    }

    public static double[] getEndPoint(final double[][] inputSegment) {
        endPoint[0] = inputSegment[1][0];
        endPoint[1] = inputSegment[1][1];
        return endPoint;
    }
}
