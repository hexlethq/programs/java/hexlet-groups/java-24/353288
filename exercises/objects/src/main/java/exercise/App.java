package exercise;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class App {
    // BEGIN
    private static final String UL = "<ul>\n";
    private static final String SLASH_UL = "</ul>";
    private static final String LI_BAD_TEST_TWO_SPACES = "  <li>";
    private static final String SLASH_LI = "</li>\n";
    public static String buildList(final String[] inputList) {
        StringBuilder stringBuildList = new StringBuilder();
        if (inputList.length != 0) {
            stringBuildList.append(UL);
            for (int i = 0; i < inputList.length; i++) {
                stringBuildList.append(LI_BAD_TEST_TWO_SPACES).append(inputList[i]).append(SLASH_LI);
            }
            stringBuildList.append(SLASH_UL);
            return stringBuildList.toString();
        } else {
            return "";
        }
    }

        public static String getUsersByYear(final String[][] users, final int year) throws Exception {
        SimpleDateFormat methodSimpleDate = new SimpleDateFormat("yyyy");
        StringBuilder stringGetUsersByYear = new StringBuilder();
        int counter = 0;
        stringGetUsersByYear.append(UL);
        for (int i = 0; i < users.length; i++) {
            if (methodSimpleDate.parse(users[i][1]).equals(methodSimpleDate.parse(String.valueOf(year)))) {
                stringGetUsersByYear.append(LI_BAD_TEST_TWO_SPACES).append(users[i][0]).append(SLASH_LI);
                counter++;
            }
        }
        if (counter == 0) {
            return "";
        }
        stringGetUsersByYear.append(SLASH_UL);
        return stringGetUsersByYear.toString();
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(final String[][] users, final String date) throws Exception {
        // BEGIN
        String result = "";
        SimpleDateFormat simpleUsersDate = new SimpleDateFormat("yyyy-MM-dd");
        Date usersDate = new Date();
        SimpleDateFormat simpleInputDate = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        Date inputDate = simpleInputDate.parse(date);
        String[][] youngerUsers = new String[users.length][users[0].length];
        int indexOfYoungerUser = 0;
        for (int i = 0; i < users.length; i++) {
            usersDate = simpleUsersDate.parse(users[i][1]);
            if (usersDate.before(inputDate)) {
                youngerUsers[indexOfYoungerUser][0] = users[i][0];
                youngerUsers[indexOfYoungerUser][1] = users[i][1];
                indexOfYoungerUser++;
            }
        }
        String[][] youngerUsersWithoutNull = new String[indexOfYoungerUser][users[0].length];
        for (int j = 0; j < youngerUsersWithoutNull.length; j++) {
            youngerUsersWithoutNull[j][0] = youngerUsers[j][0];
            youngerUsersWithoutNull[j][1] = youngerUsers[j][1];
        }
        if (youngerUsersWithoutNull.length == 1) {
            result = youngerUsersWithoutNull[0][0];
            return result;
        }
        String tempString0 = "";
        String tempString1 = "";
        if (youngerUsersWithoutNull.length > 1) {
            for (int m = 0; m < youngerUsersWithoutNull.length - 1; m++) {
                for (int n = 0; n < youngerUsersWithoutNull.length - 1 - m; n++) {
                    if (simpleUsersDate.parse(youngerUsersWithoutNull[n][1]).
                            before(simpleUsersDate.parse(youngerUsersWithoutNull[n + 1][1]))) {
                        tempString0 = youngerUsersWithoutNull[n][0];
                        tempString1 = youngerUsersWithoutNull[n][1];
                        youngerUsersWithoutNull[n][0] = youngerUsersWithoutNull[n + 1][0];
                        youngerUsersWithoutNull[n][1] = youngerUsersWithoutNull[n + 1][1];
                        youngerUsersWithoutNull[n + 1][0] = tempString0;
                        youngerUsersWithoutNull[n + 1][1] = tempString1;
                    }
                }
            }
            result = youngerUsersWithoutNull[0][0];
        }
        return result;
        // END
    }
}
