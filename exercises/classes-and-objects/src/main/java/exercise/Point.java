package exercise;

public class Point {
    public Integer xCoord;
    public Integer yCoord;

    public Point(Integer xCoord, Integer yCoord) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }

    public Integer getX() {
        return xCoord;
    }

    public Integer getY() {
        return yCoord;
    }
}
