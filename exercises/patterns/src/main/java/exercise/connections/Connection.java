package exercise.connections;

public interface Connection {
    // BEGIN
    void makeConnection();
    void makeDisconnection();
    void writeData(String data);
    String currentStateOfConnection();
    // END
}
