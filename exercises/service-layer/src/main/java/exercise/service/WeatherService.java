package exercise.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import exercise.HttpClient;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import exercise.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;


@Service
public class WeatherService {

    @Autowired
    CityRepository cityRepository;

    // Клиент
    HttpClient client;

    // При создании класса сервиса клиент передаётся снаружи
    // В теории это позволит заменить клиент без изменения самого сервиса
    WeatherService(HttpClient client) {
        this.client = client;
    }

    // BEGIN
    public Map<String, String> getWeatherByCityName(String cityName){
        String responce = client.get("http://weather/api/v2/cities/" + cityName);
        ObjectMapper ob = new ObjectMapper();
        Map<String, String> map = new HashMap<>();
        try {
            map = ob.readValue(responce, Map.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        if (map.isEmpty()) {
            throw new RuntimeException("Weather information is not available for " + cityName);
        }

        return map;
    }

    public Map<String, String> getTemperatureByCityName(String cityName) {
        Map<String, String> map = getWeatherByCityName(cityName);
        map.remove("cloudy");
        map.remove("humidity");
        map.remove("wind");
        return map;
    }
    // END
}
