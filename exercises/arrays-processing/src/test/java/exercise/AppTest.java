package exercise;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

class AppTest {
    @Test
    void testGetElements1() {
        int[] numbers1 = {};
        int[] result1 = App.getElementsLessAverage(numbers1);
        assertThat(result1).isEmpty();

        int[] numbers2 = {0, 1, 2, 3, 4, 5, 10, 12};
        int[] result2 = App.getElementsLessAverage(numbers2);
        int[] expected2 = {0, 1, 2, 3, 4};
        assertThat(result2).containsExactly(expected2);
    }

    // BEGIN
    // END
}
