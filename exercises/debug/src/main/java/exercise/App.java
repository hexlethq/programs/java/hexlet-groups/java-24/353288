package exercise;

class App {
    // BEGIN
    public static final String NOTEXIST = "Треугольник не существует";
    public static final String EQUILATERAL = "Равносторонний";
    public static final String ISOSCELES = "Равнобедренный";
    public static final String VERSATILE = "Разносторонний";

    public static String getTypeOfTriangle(final int sideA, final int sideB, final int sideC) {
        if ((sideA + sideB) <= sideC || (sideA + sideC) <= sideB || (sideB + sideC) <= sideA) {
            return NOTEXIST;
        }
        if (sideA == sideB && sideB == sideC) {
            return EQUILATERAL;
        }
        if (sideA == sideB || sideA == sideC || sideB == sideC) {
            return ISOSCELES;
        }
        return VERSATILE;
    }
}
