package exercise;

// BEGIN
public class MaxThread extends Thread {
    private int max = Integer.MIN_VALUE;
    private int[] inputArr;

    public MaxThread(int[] numbers) {
        this.inputArr = numbers;
    }

    /**
     *
     */
    @Override
    public void run() {

        for (int i : inputArr) {
            if (i > max) {
                max = i;
            }
        }

//        super.run();
    }

    /**
     * @return max
     */
    public int getMax() {
        return max;
    }
}
// END
