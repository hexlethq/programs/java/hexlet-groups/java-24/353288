package exercise;

import java.util.Arrays;


// BEGIN
public class Kennel {
    private static String[][] kennel = new String[0][2];

    public static void addPuppy(final String[] newPuppy) {
        addPlaceInKennel();
        kennel[getPuppyCount() - 1] = newPuppy;
    }

    public static void addPlaceInKennel() {
        kennel = Arrays.copyOf(kennel, getPuppyCount() + 1);
    }

    public static void addSomePuppies(final String[][] newPuppies) {
        for (int i = 0; i < newPuppies.length; i++) {
            addPuppy(newPuppies[i]);
        }
    }

    public static int getPuppyCount() {
        return kennel.length;
    }

    public static boolean isContainPuppy(final String puppyName) {
        for (int i = 0; i < getPuppyCount(); i++) {
            if (puppyName.equals(kennel[i][0])) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        return kennel;
    }

    public static String[] getNamesByBreed(final String targetBreed) {
        String[] namesByBreed = new String[0];
        for (int i = 0; i < getPuppyCount(); i++) {
            if (targetBreed.equals(kennel[i][1])) {
                namesByBreed = Arrays.copyOf(namesByBreed, namesByBreed.length + 1);
                namesByBreed[namesByBreed.length - 1] = kennel[i][0];
            }
        }
        return namesByBreed;
    }

    public static void resetKennel() {
        kennel = new String[0][2];
    }

    public static boolean removePuppy(final String targetName) {
        for (int i = 0; i < getPuppyCount(); i++) {
            if (targetName.equals(kennel[i][0])) {
                for (int j = i; j < getPuppyCount() - 1; j++) {
                    kennel[j] = kennel[j + 1];
                }
                kennel = Arrays.copyOf(kennel, getPuppyCount() - 1);
                return true;
            }
        } return false;
    }
}
// END
