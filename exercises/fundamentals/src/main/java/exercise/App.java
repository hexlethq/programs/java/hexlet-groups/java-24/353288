package exercise;

class App {
    public static void numbers() {
        // BEGIN
        int sum;
        int a = 8 / 2;
        int b = 100 % 3;
        sum = a + b;
        System.out.println(sum);
        // END
    }

    public static void strings() {
        String language = "Java";
        // BEGIN
        System.out.println(language + " works " + "on " + "JVM");
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        System.out.println(soldiersCount + " " + name);
        // END
    }
}
