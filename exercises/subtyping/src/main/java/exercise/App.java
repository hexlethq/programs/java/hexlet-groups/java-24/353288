package exercise;

import java.util.ArrayList;
import java.util.List;

// BEGIN
public class App {
    public static void swapKeyValue(KeyValueStorage storage) {
        List<String> storageList = new ArrayList<>(storage.toMap().keySet());
        for (int i = 0; i < storageList.size(); i++) {
            storage.set(storage.get(storageList.get(i), "error -1"), storageList.get(i));
            storage.unset(storageList.get(i));

        }
    }
}
// END
