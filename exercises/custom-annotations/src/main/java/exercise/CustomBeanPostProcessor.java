package exercise;

import java.lang.reflect.Proxy;

import exercise.calculator.Calculator;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.HashMap;

import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// BEGIN
@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomBeanPostProcessor.class);
    private static Map<String, String> searchedBeanWithParameter = new HashMap<>();


    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean.getClass().isAnnotationPresent(Inspect.class)) {
            String parameterOfAnnotation = bean.getClass().getAnnotation(Inspect.class).level();
            if (searchedBeanWithParameter == null) {
                searchedBeanWithParameter = new HashMap<>();
            }
            searchedBeanWithParameter.put(beanName, parameterOfAnnotation);
        }
        return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

        for (Map.Entry<String, String> entry : searchedBeanWithParameter.entrySet()) {
            if (beanName.equals(entry.getKey())) {

                return Proxy.newProxyInstance(
                        bean.getClass().getClassLoader(),
                        bean.getClass().getInterfaces(),
                        ((proxy, method, args) -> {


                            String message = String.format(
                                    "Was called method: %s() with arguments: %s",
                                    method.getName(),
                                    Arrays.toString(args)
                            );
                            if ("info".equals(entry.getValue())) {
                                LOGGER.info(message);
                            } else {
                                LOGGER.debug(message);
                            }
                            return method.invoke(bean, args);
                        })
                );
            }
        }


        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }
}
// END
