package exercise;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

class ReversedSequenceTest {

    @Test
    void testReversedSequence() {
        CharSequence cs = new ReversedSequence("Hello, world!");
        int expectedLength = 13;
        String expectedToString = "!dlrow ,olleH";
        char expectedCharAt5 = 'w';
        String expectedSubSequence8to13 = "olleH";

        assertThat(cs.length()).isEqualTo(expectedLength);
        assertThat(cs.toString()).isEqualTo(expectedToString);
        assertThat(cs.charAt(5)).isEqualTo(expectedCharAt5);
        assertThat(cs.subSequence(8, 13)).isEqualTo(expectedSubSequence8to13);
    }
}
