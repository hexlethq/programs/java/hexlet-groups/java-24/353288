package exercise;

import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String getForwardedVariables(String configInput) {
//        Arrays.stream(configInput.split("\n"))
        return configInput.lines()
                .filter(x -> x.startsWith("environment"))
                .flatMap(x -> Stream.of(x.split(",")))
                .filter(x -> x.contains("X_FORWARDED_"))
                .map(x -> x.substring(x.indexOf("X_FORWARDED_") + "X_FORWARDED_".length()))
                .map(x -> x.endsWith("\"") ? x.substring(0, x.length() - 1) : x)
                .collect(Collectors.joining(","));
    }
}

//END
