package exercise;

// BEGIN
public class Cottage implements Home {
    double area;
    int floorsCount;

    public Cottage(double area, int floorsCount) {
        this.area = area;
        this.floorsCount = floorsCount;
    }

    @Override
    public double getArea() {
        return area;
    }

    @Override
    public int compareTo(Home another) {
        return Double.compare(this.getArea(), another.getArea());
    }

    @Override
    public String toString() {
        return String.valueOf(floorsCount) + " этажный коттедж площадью " + String.valueOf(area) + " метров";
    }
}
// END
