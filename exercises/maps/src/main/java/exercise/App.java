package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class App {
    public static Map getWordCount(String inputSentence) {
        Map<String, Integer> wordCount = new HashMap<>();
        if (inputSentence.equals("")) {
            return wordCount;
        }
        for (String word : inputSentence.split(" ")) {
            if (!wordCount.containsKey(word)) {
                wordCount.put(word, 1);
            } else {
                wordCount.put(word, wordCount.get(word) + 1);
            }
        }
        return wordCount;
    }

    public static String toString(Map<String, Integer> inputMap) {
        if (inputMap.isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        for (Map.Entry<String, Integer> entry: inputMap.entrySet()) {
            sb.append("  ");
            sb.append(entry.getKey());
            sb.append(": ");
            sb.append(String.valueOf(entry.getValue()));
            sb.append("\n");
        }
        sb.append("}");
        return sb.toString();
    }
}
//END
