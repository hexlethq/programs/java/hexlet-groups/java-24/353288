package exercise;

import java.util.HashMap;
import org.junit.jupiter.api.BeforeEach;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
// BEGIN
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
// END


class FileKVTest {

    private static Path filepath = Paths.get("src/test/resources/file").toAbsolutePath().normalize();

    @BeforeEach
    public void beforeEach() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String content = mapper.writeValueAsString(new HashMap<String, String>());
        Files.writeString(filepath, content, StandardOpenOption.CREATE);
    }

    // BEGIN
    @Test
    void FileKVTest() {
        KeyValueStorage storage = new FileKV(filepath.toString(), Map.of("key1", "one"));
        assertThat(storage.get("key2", "default")).isEqualTo("default");
        assertThat(storage.get("key1", "default")).isEqualTo("one");

        storage.set("key2", "two");
        storage.set("key1", "newOne");

        assertThat(storage.get("key2", "default")).isEqualTo("two");
        assertThat(storage.get("key1", "default")).isEqualTo("newOne");

        storage.unset("key1");
        assertThat(storage.get("key1", "default")).isEqualTo("default");
        assertThat(storage.toMap()).isEqualTo(Map.of("key2", "two"));
    }

    @Test
    void mustBeImmutableTest() {
        Map<String, String> initial = new HashMap<>();
        initial.put("key1", "one");

        Map<String, String> clonedInitial = new HashMap<>();
        clonedInitial.putAll(initial);

        KeyValueStorage storage = new FileKV(filepath.toString(), initial);

        initial.put("key2", "two");
        assertThat(storage.toMap()).isEqualTo(clonedInitial);

        Map<String, String> map = storage.toMap();
        map.put("key2", "value2");
        assertThat(storage.toMap()).isEqualTo(clonedInitial);
    }
    // END
}
