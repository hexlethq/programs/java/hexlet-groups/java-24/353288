package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class FileKV implements KeyValueStorage {
    String path;
    Map<String, String> initialBase;

    public FileKV(String path, Map<String, String> initialBase) {
        this.path = String.valueOf(path);
        this.initialBase = new HashMap<>(initialBase);
        Utils.writeFile(path, Utils.serialize(this.initialBase));
    }

    @Override
    public void set(String key, String value) {
        Map<String, String> map = new HashMap<>(Utils.unserialize(Utils.readFile(path)));
        map.put(key, value);
        Utils.writeFile(path, Utils.serialize(map));

    }

    @Override
    public void unset(String key) {
        Map<String, String> map = new HashMap<>(Utils.unserialize(Utils.readFile(path)));
        map.remove(key);
        Utils.writeFile(path, Utils.serialize(map));
    }

    @Override
    public String get(String key, String defaultValue) {
        Map<String, String> map = new HashMap<>(Utils.unserialize(Utils.readFile(path)));
        if (map.containsKey(key)) {
            return map.get(key);
        }
        return defaultValue;
    }

    @Override
    public Map<String, String> toMap() {
        return new HashMap<>(Utils.unserialize(Utils.readFile(path)));
    }
}
// END
