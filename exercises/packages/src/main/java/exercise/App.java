// BEGIN
package exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;

public class App {
    public static double[] getMidpointOfSegment(final double[][] inputSegment) {
        return Point.makePoint(((inputSegment[0][0] + inputSegment[1][0]) / 2),
                ((inputSegment[0][1] + inputSegment[1][1]) / 2));
    }

    public static double[][] reverse(final double[][] inputSegment) {
        return Segment.makeSegment(Point.makePoint(inputSegment[1][0], inputSegment[1][1]),
                Point.makePoint(inputSegment[0][0], inputSegment[0][1]));
    }

    public static boolean isBelongToOneQuadrant(final double[][] inputSegment) {
        return (inputSegment[0][0] > 0 && inputSegment[0][1] > 0
                && inputSegment[1][0] > 0 && inputSegment[1][1] > 0)
                || (inputSegment[0][0] < 0 && inputSegment[0][1] < 0
                && inputSegment[1][0] < 0 && inputSegment[1][1] < 0)
                || (inputSegment[0][0] < 0 && inputSegment[0][1] > 0
                && inputSegment[1][0] < 0 && inputSegment[1][1] > 0)
                || (inputSegment[0][0] > 0 && inputSegment[0][1] < 0
                && inputSegment[1][0] > 0 && inputSegment[1][1] < 0);
    }
}
// END
