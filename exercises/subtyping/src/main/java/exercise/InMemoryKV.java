package exercise;

import java.util.Map;
import java.util.HashMap;

// BEGIN
public class InMemoryKV implements KeyValueStorage {
    Map<String, String> stringMap;

    public InMemoryKV(Map<String, String> stringMap) {
        this.stringMap = new HashMap<>();
        this.stringMap.putAll(stringMap);
    }

    @Override
    public void set(String key, String value) {
        this.stringMap.put(key, value);
    }

    @Override
    public void unset(String key) {
        this.stringMap.remove(key);
    }

    @Override
    public String get(String key, String defaultValue) {
//        if (this.stringMap.containsKey(key)) {
//            return this.stringMap.get(key);
//        }
//        return defaultValue;
        return this.stringMap.getOrDefault(key, defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        Map<String, String> tm = new HashMap<>(this.stringMap);
        return tm;
    }
}
// END
