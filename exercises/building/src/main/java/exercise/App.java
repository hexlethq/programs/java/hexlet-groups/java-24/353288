// BEGIN
package exercise;

import com.google.gson.Gson;

public class App {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }

    public static String toJson(String[] inputArray) {
        Gson gson = new Gson();
        return gson.toJson(inputArray);
    }
}
// END
