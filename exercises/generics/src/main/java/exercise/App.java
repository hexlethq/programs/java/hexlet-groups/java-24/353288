package exercise;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// BEGIN
public class App {
    public static List<Map<String, String>> findWhere(List<Map<String, String>> books, Map<String, String> where) {
        List<Map<String, String>> result = new ArrayList<>();
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).entrySet().containsAll(where.entrySet())) {
                result.add(books.get(i));
            }
        }
        return result;
    }
}
//END
