package exercise;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static exercise.Point.getSymmetricalPointByX;
import static exercise.Point.calculateDistance;

public class TestTask {
    @Test
    void testGetSymmetricalPointByX() {
        int[] basicPoint = {-5, -10};
        int[] symmetricalPointByX = {-5, 10};
        assertThat(getSymmetricalPointByX(basicPoint)).isEqualTo(symmetricalPointByX);
    }

    @Test
    void testCalculateDistance() {
        int[] pointA = {-1, 0};
        int[] pointB = {3, 3};
        int diagonal = 5;
        assertThat(calculateDistance(pointA, pointB)).isEqualTo(diagonal);
    }
}
