package exercise;

import java.io.File;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.ExecutionException;

class App {

    // BEGIN

    // END

    public static void main(String[] args) {
        // BEGIN
        CompletableFuture<String> result = App.unionFiles("src/main/resources/file1.txt",
                "src/main/resources/file2.txt",
                "src/main/resources/myResult.txt");

        CompletableFuture<Long> size = App.getDirectorySize("src/test/resources/dir");
        // END
    }

    public static CompletableFuture<String> unionFiles(String pathOne,
                                                        String pathTwo,
                                                        String pathDestination) {

        CompletableFuture<String> firstContent = CompletableFuture.supplyAsync(() -> {
            Path path1 = Path.of(pathOne).toAbsolutePath();
            String firstFileContent = null;
            try {
                firstFileContent = Files.readString(path1);
            } catch (IOException e) {
//                throw new RuntimeException("No such file found : " + path1);
                System.out.println("NoSuchFileException : " + path1);
            }
            return firstFileContent;
        });

        CompletableFuture<String> secondContent = CompletableFuture.supplyAsync(() -> {
            Path path2 = Path.of(pathTwo).toAbsolutePath();
            String secondFileContent = null;
            try {
                secondFileContent = Files.readString(path2);
            } catch (IOException e) {
//                throw new RuntimeException("No such file found : " + path2);
                System.out.println("NoSuchFileException : " + path2);
            }
            return secondFileContent;
        });

        CompletableFuture<String> writeResult = firstContent.thenCombine(secondContent,
                (firstFileContent, secondFileContent) -> {

                    Path destination = Path.of(pathDestination).toAbsolutePath();
                    try {
                        Files.writeString(destination, firstFileContent);
                        Files.writeString(destination, secondFileContent, StandardOpenOption.APPEND);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return "Concatenation result saved in " + destination + " file successfully";

                }).exceptionally(ex -> {
            System.out.println("Exception during result writing : " + ex.getMessage());
            return null;
        });

        try {
            writeResult.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return writeResult;
    }

    public static CompletableFuture<Long> getDirectorySize(String directoryPathAsString) {
        List<Path> pathsToFilesInDirectory = getPathsToFilesInDirectory(directoryPathAsString);
        List<CompletableFuture<Long>> futures = new ArrayList<>();

        for (Path p : pathsToFilesInDirectory) {
            CompletableFuture<Long> size = CompletableFuture.supplyAsync(() -> {
                long bytes = 0;
                try {
                    bytes = Files.size(p);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return bytes;
            });

            futures.add(size);
        }

        CompletableFuture<Long> result = null;

        for (CompletableFuture<Long> completableFuture : futures) {
            if (result == null) {
                result = completableFuture;
            } else {
                result = result.thenCombine(completableFuture, Long::sum);
            }
        }

        try {
            if (result != null) {
                result.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return result;
    }

    private static List<Path> getPathsToFilesInDirectory(String directoryPathAsString) {
        List<Path> pathsToFilesInDirectory = new ArrayList<>();

        Path directoryPath = Path.of(directoryPathAsString).toAbsolutePath();
        File directory = new File(String.valueOf(directoryPath));
        File[] directoryListing = directory.listFiles();

        if (directoryListing == null) {
            throw new RuntimeException("Wrong directory path : " + directoryPath);
        }

        for (File file : directoryListing) {
            if (! file.isDirectory()) {
                Path p = Path.of(file.getAbsolutePath());
                pathsToFilesInDirectory.add(p);
            }
        }

        return pathsToFilesInDirectory;
    }

}

