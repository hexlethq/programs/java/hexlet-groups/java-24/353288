package exercise;

class Triangle {
    // BEGIN

    public static double getSquare(final int sideA, final int sideB, final double angleDegree) {
        final int const180 = 180;
        return (1.0 / 2.0) * sideA * sideB * Math.sin(angleDegree * Math.PI / const180);
    }
    public static void main(String[] args) {
        final int a = 4;
        final int b = 5;
        final double ang = 45;
        System.out.println(getSquare(a, b, ang));
    }
    // END
}
