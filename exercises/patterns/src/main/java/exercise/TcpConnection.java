package exercise;

import exercise.connections.Connection;
import exercise.connections.Disconnected;

// BEGIN
public class TcpConnection {
    String ipAddress;
    int port;
    private Connection connection;

    public TcpConnection(String ipAddress, int port) {
        this.ipAddress = ipAddress;
        this.port = port;
        this.connection = new Disconnected(this);
    }

    public void setState(Connection state) {
        this.connection = state;
    }

    public String getCurrentState() {
        return this.connection.currentStateOfConnection();
    }

    public void connect() {
        this.connection.makeConnection();
    }

    public void disconnect() {
        this.connection.makeDisconnection();
    }

    public void write(String data) {
        this.connection.writeData(data);
    }

}
// END
