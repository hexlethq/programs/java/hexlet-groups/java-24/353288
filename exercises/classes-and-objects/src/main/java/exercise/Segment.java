package exercise;

public class Segment {
    public Point beginPoint;
    public Point endPoint;

    public Segment(Point beginPoint, Point endPoint) {
        this.beginPoint = beginPoint;
        this.endPoint = endPoint;
    }

    public Point getBeginPoint() {
        return this.beginPoint;
    }

    public Point getEndPoint() {
        return this.endPoint;
    }

    public Point getMidPoint() {
        Point p = new Point((beginPoint.xCoord + endPoint.xCoord) / 2,
                (beginPoint.yCoord + endPoint.yCoord) / 2);
        return p;
    }
}
