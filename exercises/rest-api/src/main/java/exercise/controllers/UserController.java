package exercise.controllers;

import io.javalin.core.validation.ValidationError;
import io.javalin.http.Context;
import io.javalin.apibuilder.CrudHandler;
import io.ebean.DB;
import java.util.List;
import java.util.Map;

import exercise.domain.User;
import exercise.domain.query.QUser;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

public class UserController implements CrudHandler {

    public void getAll(Context ctx) {
        // BEGIN
        List<User> users = new QUser()
                .orderBy()
                .id.asc()
                .findList();

        String json = DB.json().toJson(users);

        ctx.json(json);
        // END
    };

    public void getOne(Context ctx, String id) {

        // BEGIN
        User user = new QUser()
                .id.equalTo(Integer.parseInt(id))
                .findOne();

        String json = DB.json().toJson(user);

        ctx.json(json);
        // END
    };

    public void create(Context ctx) {

        // BEGIN
        Map<String, List<ValidationError<User>>> errors = ctx.bodyValidator(User.class)
                .check(value -> !value.getFirstName().isEmpty(), "'First name' should not be empty")
                .check(value -> !value.getLastName().isEmpty(), "'Last name' should not be empty")
                .check(value -> EmailValidator.getInstance().isValid(value.getEmail()), "Email should be valid")
                .check(value -> StringUtils.isNumeric(value.getPassword()), "Password should contain only digits")
                .errors();

        String body = ctx.body();
        User user = DB.json().toBean(User.class, body);

        if (!errors.isEmpty()) {
            ctx.status(422);
            ctx.json(errors);
            return;
        }

        user.save();
        // END
    };

    public void update(Context ctx, String id) {
        // BEGIN
        String body = ctx.body();
        User user = DB.json().toBean(User.class, body);

        user.setId(id);
        user.update();
        // END
    };

    public void delete(Context ctx, String id) {
        // BEGIN
        User user = new User();
        user.setId(id);
        user.delete();
        // END
    };
}