package exercise;

import java.util.Map;

// BEGIN
public class SingleTag extends Tag {

    SingleTag(String name, Map<String, String> attributes) {
        super(name, attributes);
    }

    @Override
    public String toString() {
        String result = "<" + name + " ";
        for (Map.Entry entry : attributes.entrySet()) {
            result = result + entry.getKey() + "=\"" + entry.getValue() + "\" ";
        }
        result = result.substring(0, result.length() - 1) + ">";
        return result;
    }
}
// END
