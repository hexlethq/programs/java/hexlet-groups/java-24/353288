package exercise.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        List<Map<String, String>> users = new ArrayList<Map<String, String>>(getUsers());

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        File file = new File("src/main/resources/users.json");
        ObjectMapper ob = new ObjectMapper();
        CollectionType mapCollectionType = ob.getTypeFactory().constructCollectionType(List.class, Map.class);
        return ob.<List<Map<String, String>>>readValue(file, mapCollectionType);
        // END
    }

    private void showUsers(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException {

        // BEGIN
    PrintWriter pw = response.getWriter();
    List<Map<String, String>> users = new ArrayList<Map<String, String>>(getUsers());
    StringBuilder body = new StringBuilder();
    body.append(
            "<!DOCTYPE html>" +
                    "<html>" +
                        "<head>" +
                            "<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css\"" +
                            "rel=\"stylesheet\"" +
                            "integrity=\"sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We\"" +
                            "crossorigin=\"anonymous\">\n" +
                            "<style>" +
                                "table, th, td {border: 1px solid black;}" +
                            "</style>" +
                        "</head>" +
                            "<body>" +
                                "<table>" +
                                    "<tr>" + "<th>" + "User ID" + "</th>" +
                                    "<th>" + "Full Name" + "</th>" + "</tr>");
    for (int i = 0; i < users.size(); i++) {
        String fullName = users.get(i).get("firstName") + " " + users.get(i).get("lastName");
        body.append(
                                    "<tr>" +
                                            "<td>" + users.get(i).get("id") + "</td>" +
                                            "<td>" + "<a href='/users/" + users.get(i).get("id") + "'>" +
                                                    fullName + "</a>" + "</td>" +
                                    "</tr>");
    }
    body.append(
                                "</table>" +
                            "</body>" +
                        "</html>");
    pw.write(body.toString());
        // END
    }

    private void showUser(HttpServletRequest request,
                         HttpServletResponse response,
                         String id)
                 throws IOException {

        // BEGIN
        List<Map<String, String>> users = new ArrayList<Map<String, String>>(getUsers());
        PrintWriter pw = response.getWriter();
        Map<String, String> user = users.stream()
                .filter(u -> u.containsValue(id))
                .findAny()
                .orElse(null);
        if (user == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        } else {
            StringBuilder body = new StringBuilder();
            body.append(
                    "<!DOCTYPE html>" +
                            "<html>" +
                                "<head>" +
                                        "<link href=\"" +
                                        "https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css\"" +
                                        "rel=\"stylesheet\"" +
                                        "integrity=\"" +
                                        "sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We\"" +
                                        "crossorigin=\"anonymous\">\n" +
                                    "<style>" +
                                        "table, th, td {border: 1px solid black;}" +
                                    "</style>" +
                                "</head>" +
                                "<body>" +
                                    "<table>" +
                                        "<tr>" +
                                            "<th>" + "User ID" + "</th>" +
                                            "<th>" + "First name" + "</th>" +
                                            "<th>" + "Last name" + "</th>" +
                                            "<th>" + "Email" + "</th>" +
                                        "</tr>");
            body.append(
                                        "<tr>" +
                                                "<td>" + user.get("id") + "</td>" +
                                                "<td>" + user.get("firstName") + "</td>" +
                                                "<td>" + user.get("lastName") + "</td>" +
                                                "<td>" + user.get("email") + "</td>" +
                                        "</tr>");
            body.append(
                                    "</table>" +
                               "</body>" +
                            "</html>"
            );
            pw.write(body.toString());
        }
        // END
    }
}
