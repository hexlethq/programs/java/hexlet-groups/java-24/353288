package exercise;

import java.util.Map;
import java.util.logging.Logger;

class App {
    private static final Logger LOGGER = Logger.getLogger("AppLogger");

    // BEGIN
    public static Map<String, Integer> getMinMax(int[] inputArr) {
        MinThread minThread = new MinThread(inputArr);
        MaxThread maxThread = new MaxThread(inputArr);
        minThread.start();
        LOGGER.info(minThread.getName() + " started");
        maxThread.start();
        LOGGER.info(maxThread.getName() + " started");
        try {
            minThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            maxThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Map<String, Integer> minAndMax = Map.of("max", maxThread.getMax(), "min", minThread.getMin());
        LOGGER.info(maxThread.getName() + " finished");
        LOGGER.info(minThread.getName() + " finished");
        return minAndMax;

    }
    // END
}

