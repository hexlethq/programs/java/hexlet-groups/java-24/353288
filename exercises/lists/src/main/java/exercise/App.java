package exercise;

import java.util.ArrayList;
import java.util.List;

// BEGIN
public class App {

    public static boolean scrabble(String symbols, String word) {
        int counter = 0;
        if (word.length() > symbols.length()) {
            return false;
        }
        String symbolsLowerCase = symbols.toLowerCase();
        String wordLowerCase = word.toLowerCase();
        List<Character> symbolsCharacter = stringToCharList(symbolsLowerCase);
        List<Character> wordCharacter = stringToCharList(wordLowerCase);
        for (Character character : wordCharacter) {
            for (int j = 0; j < symbolsCharacter.size(); j++) {
                if (character == symbolsCharacter.get(j)) {
                    symbolsCharacter.remove(j);
                    counter++;
                    break;
                }
            }
        }
        return counter == wordCharacter.size();
    }

    public static List<Character> stringToCharList(String inputString) {
        List<Character> inputList = new ArrayList<>();
        for (char c : inputString.toCharArray()) {
            inputList.add(c);
        }
        return inputList;
    }
}
//END
