package exercise;

class Point {
    // BEGIN
    private static int[] point = new int[2];
    private static int quadrant = -1;
    private static final int NO_QUADRANT = 0;
    private static final int FIRST_QUADRANT = 1;
    private static final int SECOND_QUADRANT = 2;
    private static final int THIRD_QUADRANT = 3;
    private static final int FOURTH_QUADRANT = 4;

    public static int[] makePoint(final int coordinateX, final int coordinateY) {
        point[0] = coordinateX;
        point[1] = coordinateY;
        return point;
    }

    public static int getX(final int[] pointToGetX) {
        return pointToGetX[0];
    }

    public static int getY(final int[] pointToGetY) {
        return pointToGetY[1];
    }

    public static String pointToString(final int[] pointToString) {
        return "(" + Integer.toString(pointToString[0]) + ", " + Integer.toString(pointToString[1]) + ")";
    }

    public static int getQuadrant(final int[] inputPoint) {
        if (inputPoint[0] == 0 || inputPoint[1] == 0) {
            quadrant = NO_QUADRANT;
        }
        if (inputPoint[0] > 0 && inputPoint[1] > 0) {
            quadrant = FIRST_QUADRANT;
        }
        if (inputPoint[0] < 0 && inputPoint[1] > 0) {
            quadrant = SECOND_QUADRANT;
        }
        if (inputPoint[0] < 0 && inputPoint[1] < 0) {
            quadrant = THIRD_QUADRANT;
        }
        if (inputPoint[0] > 0 && inputPoint[1] < 0) {
            quadrant = FOURTH_QUADRANT;
        }
        return quadrant;
    }

    public static int[] getSymmetricalPointByX(final int[] pointForSymmetry) {
        pointForSymmetry[1] = pointForSymmetry[1] * (-1);
        return pointForSymmetry;
    }

    public static int calculateDistance(final int[] point1, final int[] point2) {
        int sideSizeA = getSideSize(point1[0], point2[0]);
        int sideSizeB = getSideSize(point1[1], point2[1]);
        double resultDouble = Math.sqrt(Math.pow(sideSizeA, 2) + Math.pow(sideSizeB, 2));
        return (int) resultDouble;
    }

    public static int getSideSize(final int start, final int end) {
        int sideSize = end - start;
        if (sideSize < 0) {
            sideSize = sideSize * (-1);
        }
        return sideSize;
    }

    // END
}
