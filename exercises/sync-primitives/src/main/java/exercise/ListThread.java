package exercise;

import java.util.Random;

// BEGIN
public class ListThread extends Thread{
    private SafetyList safetyList;
    private Random random;

    public ListThread(SafetyList safetyList) {
        this.safetyList = safetyList;
    }

    @Override
    public void run() {
        this.random = new Random();
        for (int i = 0; i < 1000; i++) {
            synchronized(this) {
                try {
                    wait(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            this.safetyList.add(this.random.nextInt());
        }
    }
}
// END
