package exercise;

public class NegativeRadiusExceptions {
    private final String exception;

    public NegativeRadiusExceptions(String exception) {
        this.exception = exception;
    }
}
