<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Edit user</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
            crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <a href="/users">Все пользователи</a>
            <!-- BEGIN -->
            <form method="post" action="edit?id=${user.get("id")}">
                <div class="mb-3">
                    <label>First name:</label>
                    <input class="form-control" type="text" name="firstName" value=${user.get("firstName")}>
                    <br>
                    <label>Last name:</label>
                    <input class="form-control" type="text" name="lastName" value=${user.get("lastName")}>
                    <br>
                    <label>Email:</label>
                    <input class="form-control" type="text" name="email" value=${user.get("email")}
                            <br>
                </div>
                <button class="btn btn-primary" type="submit">Сохранить</button>
                <input class="btn btn-primary" type="button" onclick="window.location.replace('show?id=${user.get("id")}')" value="Отмена" />
            </form>
            <p style="color:red">
                ${errorMsg}
            </p>
            <!-- END -->
        </div>
    </body>
</html>
