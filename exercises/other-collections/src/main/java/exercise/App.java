package exercise;

import java.util.*;

// BEGIN
public class App {
    public static LinkedHashMap<String, String> genDiff(Map<String, Object> firstDic, Map<String, Object> secondDic) {
        Map<String, String> genDiffTree = new TreeMap<>();
        if (firstDic.isEmpty() && !secondDic.isEmpty()) {
            for (Map.Entry<String, Object> second : secondDic.entrySet()) {
                genDiffTree.put(second.getKey(), "added");
            }
        }
        if (secondDic.isEmpty() && !firstDic.isEmpty()) {
            for (Map.Entry<String, Object> first : firstDic.entrySet()) {
                genDiffTree.put(first.getKey(), "deleted");
            }
        }
        for (Map.Entry<String, Object> first : firstDic.entrySet()) {
            for (Map.Entry<String, Object> second : secondDic.entrySet()) {
                if (first.equals(second)) {
                    genDiffTree.put(first.getKey(), "unchanged");
                }
                if (secondDic.get(first.getKey()) == null) {
                    genDiffTree.put(first.getKey(), "deleted");
                }
                if (firstDic.get(second.getKey()) == null) {
                    genDiffTree.put(second.getKey(), "added");
                }
                if (first.getKey().equals(second.getKey()) && !first.getValue().equals(second.getValue())) {
                    genDiffTree.put(first.getKey(), "changed");
                }
            }
        }
        return new LinkedHashMap<>(genDiffTree);
    }
}
//END
