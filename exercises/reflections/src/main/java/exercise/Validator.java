package exercise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// BEGIN
public class Validator {
    public static List<String> validate(Address address) {
        List<String> notValidFields = new ArrayList<>();
        for (Field field: address.getClass().getDeclaredFields()) {
            NotNull notNull = field.getAnnotation(NotNull.class);
            if (notNull != null) {
                field.setAccessible(true);
                try {
                    if (field.get(address) == null) {
                        notValidFields.add(field.getName());
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return notValidFields;
    }

    public static Map<String, List<String>> advancedValidate(Address address) {
        Map<String, List<String>> notValidFields = new HashMap<>();
        for (Field field: address.getClass().getDeclaredFields()) {
            NotNull notNull = field.getAnnotation(NotNull.class);
            MinLength minLength = field.getAnnotation(MinLength.class);
            if (notNull != null) {
                field.setAccessible(true);
                try {
                    if (field.get(address) == null) {
                        listInMapSynchronization(notValidFields,
                                String.valueOf(field.getName()), "can not be null");
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            if (minLength != null) {
                field.setAccessible(true);
                try {
                    if (String.valueOf(field.get(address)).length() < Address.FIVE_IS_NOT_MAGIC) {
                            listInMapSynchronization(notValidFields, String.valueOf(field.getName()),
                                    "length less than 5");
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return notValidFields;
    }

    public static Map<String, List<String>> listInMapSynchronization(Map<String,
            List<String>> inputMap, String key, String message) {
        if (inputMap.get(key) == null) {
            inputMap.put(key, List.of(message));
        } else {
            List<String> newList = new ArrayList<>(inputMap.get(key));
            newList.add(message);
            inputMap.put(key, newList);
        }
        return inputMap;
    }
}
// END
