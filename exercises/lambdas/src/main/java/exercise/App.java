package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String[][] enlargeArrayImage(String[][] basicImage) {
        return Arrays.stream(basicImage)
                .flatMap(e -> Stream.of(doubleElements(e), doubleElements(e)))
                .toArray(String[][]::new);
    }

    public static String[] doubleElements(String[] basicArray) {
        return Arrays.stream(basicArray)
                .flatMap(e -> Stream.of(e, e))
                .toArray(String[]::new);
    }

}
// END
