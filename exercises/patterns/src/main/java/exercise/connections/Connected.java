package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public class Connected implements Connection {
    private TcpConnection tcpConnection;

    public Connected(TcpConnection tcpConnection) {
        this.tcpConnection = tcpConnection;
    }

    @Override
    public String currentStateOfConnection() {
        return "connected";
    }

    @Override
    public void makeConnection() {
        System.out.println("Error! Already connected");
    }

    @Override
    public void makeDisconnection() {
        this.tcpConnection.setState(new Disconnected(this.tcpConnection));
    }

    @Override
    public void writeData(String data) {

    }
}
// END
