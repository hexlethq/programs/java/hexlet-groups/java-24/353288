package exercise;

class App {
    // BEGIN
    public static int[] sort(final int[] inputArray) {
        for (int i = 0; i < (inputArray.length - 1); i++) {
            for (int k = 0; k < (inputArray.length - i - 1); k++) {
                if (inputArray[k] > inputArray[k + 1]) {
                    int temp = inputArray[k];
                    inputArray[k] = inputArray[k + 1];
                    inputArray[k + 1] = temp;
                }
            }
        }
        return inputArray;
    }

    public static int[] sortBySelection(final int[] inputArray) {
        for (int i = 0; i < inputArray.length; i++) {
            int indexOfFoundMin = i;
            int min = inputArray[i];
            for (int k = i + 1; k < inputArray.length; k++) {
                if (min > inputArray[k]) {
                    indexOfFoundMin = k;
                    min = inputArray[k];
                }
            }
            inputArray[indexOfFoundMin] = inputArray[i];
            inputArray[i] = min;
        }
        return inputArray;
    }
    // END
}
