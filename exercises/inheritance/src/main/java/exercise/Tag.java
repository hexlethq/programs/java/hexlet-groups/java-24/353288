package exercise;

import java.util.Map;

// BEGIN
class Tag {

    public String name;
    public Map<String, String> attributes;

    Tag(String name, Map<String, String> attributes) {
        this.name = name;
        this.attributes = attributes;
    }
}
// END
