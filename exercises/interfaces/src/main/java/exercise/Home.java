package exercise;

// BEGIN
public interface Home {
    double getArea();
    String toString();
    int compareTo(Home another);
}
// END
