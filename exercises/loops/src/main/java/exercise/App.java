package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String phrase) {
        String abbreviation = "";
        String[] splitText = phrase.split(" ");
        for (int i = 0; i < splitText.length; i++) {
            abbreviation = abbreviation + splitText[i].substring(0, 1).toUpperCase();
        }
        return abbreviation;
    }
    // END
}
