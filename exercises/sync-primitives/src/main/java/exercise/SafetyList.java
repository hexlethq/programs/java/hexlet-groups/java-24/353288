package exercise;

import java.util.ArrayList;
import java.util.List;

class SafetyList {
    private List<Integer> list;

    // BEGIN
    public synchronized void add(int input) {
        if (list == null) {
            list = new ArrayList<>();
        }
        list.add(input);
    }

    public int get(int index) {
        return list.get(index);
    }

    public int getSize() {
        return list.size();
    }
    // END
}
