package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        if (cardNumber.length() >= 4) {
            String star = "*";
            String hiddenSymbols = star.repeat(starsCount);
            String lastFour = cardNumber.substring(cardNumber.length() - 4);
            System.out.println(hiddenSymbols + lastFour);
        } else {
            System.out.println("Wrong card number");
        }
        // END
    }
}
