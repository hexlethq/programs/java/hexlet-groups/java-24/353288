// Аннотация @SpringBootApplication содержит в себе аннотацию @Configuration
// Конфигурацию можно объявить и в отдельном классе
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    // Отмечаем, что метод создаёт бин
    // Метод будет вызван и в контекст добавится бин с именем "commandLineRunner"
    @Bean
    // В метод передаётся контекст приложения ApplicationContext
    // При старте приложения в консоль будут выведены все бины, который добавились в контекст
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

            System.out.println("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                System.out.println(beanName);
            }
        };
    }
}
