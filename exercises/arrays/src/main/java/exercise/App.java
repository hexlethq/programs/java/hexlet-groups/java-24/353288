package exercise;

class App {
    // BEGIN
    public static int[] reverse(final int[] reverseArray) {
        if (reverseArray.length == 0) {
            return reverseArray;
        }
        for (int i = 0; i < reverseArray.length / 2; i++) {
            int tempInt = reverseArray[i];
            reverseArray[i] = reverseArray[reverseArray.length - i - 1];
            reverseArray[reverseArray.length - i - 1] = tempInt;
        }
        return reverseArray;
    }


    public static int getIndexOfMaxNegative(final int[] inputArray) {
        int counterOfNegative = 0;
        int maxNegative = 0;
        int indexOfMaxNegative = 0;
        for (int i = 0; i < inputArray.length; i++) {
            if (inputArray[i] < 0) {
                counterOfNegative++;
                if (maxNegative == 0 || inputArray[i] > maxNegative) {
                    maxNegative = inputArray[i];
                    indexOfMaxNegative = i;
                }
            }
        }
        if (inputArray.length == 0 || counterOfNegative == 0) {
            return -1;
        }
        return indexOfMaxNegative;
    }
    // END
}
